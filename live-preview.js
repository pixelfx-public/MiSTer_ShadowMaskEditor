(function(mask_editor){
    if (!mask_editor || document.location.search.indexOf('livePreview=true') == -1) {
        return;
    }

    try {
        document.querySelector('h1').insertAdjacentHTML('beforeend',
            ' by Tonurics'
        );

        document.querySelector('.previewType').insertAdjacentHTML('beforeend', 
            '<div><input id="livePreview" type="checkbox"><label for="livePreview">Live Preview</label></div>'
        );
        document.querySelector('.help').previousElementSibling.querySelector('div:last-child').insertAdjacentHTML('beforeend', 
            '<div><h2>Save Mask</h2><input type="text" id="saveMaskName"> <button id="saveMaskBtn">Save</button></div>'
        );

        document.querySelectorAll("div h2").forEach(function(e) {
            if (e.innerText == 'Instructions') {
                e.nextElementSibling.innerHTML = '<li>Create/Upload a mask with this editor.</li>'
                + '<li>If <em>Live Preview</em> is checked, the mask is automatically previewed on your N64Digital.</li>'
                + '<li>To save a mask on your N64Digital, enter a name under <em>Save Mask</em> and hit <em>Save</em>.</li>'
                + '<li>On N64Digital you can either select the saved mask using the OSD <em>Preset</em> menu or the <em>Video->Retro FX</em> menu.</li>';
            }
            if (e.innerText == 'Additional Information') {
                e.parentElement.insertAdjacentHTML('beforeend',
                    '<p>Based on <a style="color:white;" target="_blank" href="https://github.com/tonurics/MiSTer_ShadowMaskEditor">MiSTer Shadow Mask Editor</a> by Tonurics</p>'
                    + '<p><a style="color:white;" target="_blank" href="https://gitlab.com/pixelfx-public/MiSTer_ShadowMaskEditor">Live Preview Extension</a> by Pixel FX</p>'
                );
            }
        });

        document.querySelector(".backgroundType").insertAdjacentHTML('afterend',
            '<div class="offsetProps"><div><label for="maskOffsetX">OffsetX</label><input type="number" min="0" max="15" value="0" step="1" id="maskOffsetX"></div>'
            + '<div><label for="maskOffsetY">OffsetY</label><input type="number" min="0" max="15" value="0" step="1" id="maskOffsetY"></div></div>'
        );

        var elms = document.getElementsByTagName("*");
        var len = elms.length;
        for(var ii = 0; ii < len; ii++) {
            if (elms[ii].tagName == "SCRIPT" || elms[ii].tagName == "STYLE") {
                continue;
            }
            var myChildren = elms[ii].childNodes;
            var len2 = myChildren.length;
            for (var jj = 0; jj < len2; jj++) {
                if (myChildren[jj].nodeType === 3) {
                    myChildren[jj].nodeValue = myChildren[jj].nodeValue.replace(/MiSTer/,"FPGA");
                    myChildren[jj].nodeValue = myChildren[jj].nodeValue.replace(/Shadow Mask Editor/, "Shadow / Slot Mask Editor");
                }
            }
        }
    } catch (e) {
        alert("live-preview.js seems to be incompatible with this shadow mask creator!");
        return;
    }

    mask_editor.livePreviewState = false;
    mask_editor.__init = mask_editor.init;
    mask_editor.init = function() {
        document.getElementById('livePreview').addEventListener('change',mask_editor.previewRender);
        document.getElementById('saveMaskBtn').addEventListener('click', mask_editor.saveMaskToDevice);
        document.getElementById('maskOffsetX').addEventListener('change', mask_editor.previewRender);
        document.getElementById('maskOffsetY').addEventListener('change', mask_editor.previewRender);
        mask_editor.livePreviewState = document.getElementById('livePreview').checked = true;
        mask_editor.__init();
    }
    mask_editor.__previewRender = mask_editor.previewRender;
    mask_editor.previewRender = function() {
        mask_editor.__previewRender();
        mask_editor.livePreview();
    }
    mask_editor.livePreviewInProgress = false;
    mask_editor.livePreviewQueue = [];
    mask_editor.livePreview = function() {
        var data = document.getElementById('output').value;
        var meta = document.getElementById('previewRotate').checked ? "mrotate," : "";

        if(!document.getElementById('livePreview').checked) {
            if (!mask_editor.livePreviewState) {
                return;
            } else {
                data = "# reset";
                meta = "reset,";
            }
        }

        mask_editor.livePreviewState = document.getElementById('livePreview').checked;
        mask_editor.livePreviewQueue.push({ 'data' : data, 'meta' : meta });
        mask_editor.triggerPreview();
    }
    mask_editor.triggerPreview = function() {
        if (mask_editor.livePreviewInProgress) {
            return;
        }
        var obj = mask_editor.livePreviewQueue.shift();
        if (!obj) {
            return;
        }
        mask_editor.livePreviewInProgress = true;

        var maskWidth = document.getElementById("maskWidth").value;
        var maskHeight = document.getElementById("maskHeight").value;
        var maskOffsetXElem = document.getElementById("maskOffsetX");
        var maskOffsetYElem = document.getElementById("maskOffsetY");

        maskOffsetXElem.value = Math.min(maskWidth-1, maskOffsetXElem.value);
        maskOffsetYElem.value = Math.min(maskHeight-1, maskOffsetYElem.value);

        var data = "# offset: " + (maskWidth - maskOffsetXElem.value) + "," + (maskHeight - maskOffsetYElem.value) + "\n" + obj.data;
        var meta = obj.meta;
        var xhttp = new XMLHttpRequest();
        xhttp.onloadstart = function (e) { }
        xhttp.onloadend = function (e) { }
        xhttp.onreadystatechange = function() {
            if (xhttp.readyState == 4) {
                mask_editor.livePreviewInProgress = false;
                if (xhttp.status == 200) {
                    setTimeout(mask_editor.triggerPreview, 0);
                } else if (xhttp.status == 0) {
                    alert("Server closed the connection abruptly!");
                } else {
                    alert(xhttp.status + " Error!\n" + xhttp.responseText);
                }
            }
        };
        xhttp.open("POST", "/sm-live-preview", true);
        if (meta) {
            xhttp.setRequestHeader("X-Meta", meta);
        }
        xhttp.send(data);
    }
    mask_editor.saveMaskInProgress = false;
    mask_editor.saveMaskToDevice = function() {
        if (mask_editor.saveMaskInProgress) {
            return;
        }
        var maskWidth = document.getElementById("maskWidth").value;
        var maskHeight = document.getElementById("maskHeight").value;
        var maskOffsetXElem = document.getElementById("maskOffsetX");
        var maskOffsetYElem = document.getElementById("maskOffsetY");

        var data = "# offset: " + (maskWidth - maskOffsetXElem.value) + "," + (maskHeight - maskOffsetYElem.value) + "\n" + document.getElementById('output').value;
        var dname = document.getElementById('saveMaskName').value.trim();
        if (!dname) {
            alert("Please specify a name for the preset to create!");
            return;
        }
        mask_editor.saveMaskInProgress = true;

        var xhttp = new XMLHttpRequest();
        xhttp.onloadstart = function (e) { }
        xhttp.onloadend = function (e) { }
        xhttp.onreadystatechange = function() {
            if (xhttp.readyState == 4) {
                if (xhttp.status == 200) {
                    if (xhttp.responseText == "OK") {
                        alert("Mask successfully saved as preset: User/Slotmask Import/" + dname);
                    } else {
                        alert(xhttp.responseText);
                    }
                } else if (xhttp.status == 0) {
                    alert("Server closed the connection abruptly!");
                } else {
                    alert(xhttp.status + " Error!\n" + xhttp.responseText);
                }
                mask_editor.saveMaskInProgress = false;
            }
        };
        xhttp.open("POST", "/sm-live-preview", true);
        xhttp.setRequestHeader("X-Meta", "save,");
        xhttp.setRequestHeader("X-Meta-DN", dname);
        xhttp.send(data);
    }
})(mask_editor);
